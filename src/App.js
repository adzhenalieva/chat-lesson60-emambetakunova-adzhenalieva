import React, {Component} from 'react';
import Post from './components/Post/Post'
import SendPost from './components/SendPost/SendPost';

import './App.css';

class App extends Component {

    state = {
        posts: [],
        text: 'text message',
        author: 'author name',
        lastDate: null,
        intervalId: null
    };

    loadData = () => {
        let oldPosts = [...this.state.posts];
        const lastDateTime = this.state.lastDate;
        const urlParams = lastDateTime ? '?datetime=' + lastDateTime : '';
        fetch('http://146.185.154.90:8000/messages' + urlParams).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        }).then(posts => {
            if(posts.length === 0) {
                console.log('there is no new posts');
            } else {
                let last = posts.length - 1;
                const newPosts = posts.map(post => {
                    return {
                        time: post.datetime,
                        author: post.author,
                        text: post.message
                    };
                });
                let lastDate = posts[last].datetime;
                const allPosts = [...newPosts, ...oldPosts];
                this.setState({posts: allPosts, lastDate});
            }
        }).catch(error => {
            console.log(error);
        });
    };

    componentDidMount() {
        this.loadData();
        const intervalId = setInterval(this.loadData, 5000);
        this.setState({intervalId})
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId)
    };

    saveMessage = (event) => {
        let text = event.target.value;
        this.setState({text});
    };

    saveAuthor = (event) => {
        let author = event.target.value;
        this.setState({author});
    };

    changeValue = () => {
        this.setState({text: ''})
    };

    changeValueAuthor = () => {
        this.setState({author: ''})
    };

    sendMessage = () => {

        const url = 'http://146.185.154.90:8000/messages';
        const data = new URLSearchParams();

        let text = this.state.text;
        let author = this.state.author;

        data.set('message', text);
        data.set('author', author);

        fetch(url, {
            method: 'post',
            body: data,
        }).then(() => {
            console.log('Message sent successfully');
        }).catch(error => {
            console.log(error);
        });
        this.setState({
            text: 'text message',
            author: 'author name'
        })
    };

    getTime = (response) => {
        let datetime1 = response;
        let date = datetime1.split('T')[0];
        let minutes = datetime1.split('T')[1];
        minutes = minutes[0] + minutes[1] + minutes[2] + minutes[3] + minutes[4];
        return date + ' ' + minutes;
    };

    render() {
        return (
            <div className="App">
                <SendPost value={this.state.text}
                          valueAuthor={this.state.author}
                          onChange={event => this.saveMessage(event)}
                          change={event => this.saveAuthor(event)}
                          onClick={this.sendMessage}
                          onFocus={this.changeValue}
                          focus={this.changeValueAuthor}/>
                {this.state.posts.map((post, id) => (
                    <Post
                        key={id}
                        time={this.getTime(post.time)}
                        author={post.author}
                        text={post.text}
                    />
                ))}

            </div>
        );
    }
}

export default App;