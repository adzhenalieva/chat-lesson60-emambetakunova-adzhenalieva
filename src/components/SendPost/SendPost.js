import React, {Component} from 'react';
import './SendPost.css';

class SendPost extends Component {
    render() {
        return (
            <div>
                <input type="text" className="TextInput" onChange={this.props.onChange} value={this.props.value}
                       onFocus={this.props.onFocus}/>
                <input type="text" className="AuthorInput" onChange={this.props.change} value={this.props.valueAuthor}
                       onFocus={this.props.focus}/>
                <button className="Button" onClick={this.props.onClick}>Send</button>
            </div>
        );
    }
}

export default SendPost;